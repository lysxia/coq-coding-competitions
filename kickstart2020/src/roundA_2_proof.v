From Coq Require Import
  NArith
  List
  Streams.
From SimpleIO Require Import SimpleIO.
From Printf Require Import Printf.
From Equations Require Import Equations.

From Lib Require Import common.
From Coding Require Import roundA_2.

Import ListNotations.
Import IO.Notations.

Local Open Scope N_scope.

Declare Scope stream_scope.
Local Open Scope stream_scope.
Infix "::" := Cons : stream_scope.

Inductive select_prefix {A} : list A -> Type :=
| select_none ps : select_prefix ps
| select_one p ps : select_prefix ps -> select_prefix (p :: ps)
.

Inductive select_prefixes {A} : list (list A) -> Type :=
| select_prefixes_nil : select_prefixes []
| select_prefixes_cons ps pss
  : select_prefix ps -> select_prefixes pss -> select_prefixes (ps :: pss)
.

Fixpoint size_prefix {A} {ps : list A} (xs : select_prefix ps) : N :=
  match xs with
  | select_none _ => 0
  | select_one _ _ xs => 1 + size_prefix xs
  end.

Fixpoint size {A} {pss : list (list A)} (xss : select_prefixes pss) : N :=
  match xss with
  | select_prefixes_nil => 0
  | select_prefixes_cons _ _ xs xss => size_prefix xs + size xss
  end.

Fixpoint value_prefix {ps : list N} (xs : select_prefix ps) : N :=
  match xs with
  | select_none _ => 0
  | select_one p _ xs => p + value_prefix xs
  end.

Fixpoint value_prefixes {pss : list (list N)} (xss : select_prefixes pss) : N :=
  match xss with
  | select_prefixes_nil => 0
  | select_prefixes_cons _ _ xs xss => value_prefix xs + value_prefixes xss
  end.

(* [y] is the maximum value of [nP] plates
   from a set of plates stacks [plates]. *)
Definition optimum (plates : list (list N)) (nP : N) (y : N) : Prop :=
  ( forall xss' : select_prefixes plates,
    size xss' <= nP ->
    value_prefixes xss' <= y)
  /\
  ( exists xss : select_prefixes plates,
    size xss <= nP
    /\ value_prefixes xss = y).

Definition CORRECT : Prop :=
  forall (nP : N) (plates : list (list N)),
  optimum plates nP (best_value nP plates).

CoInductive ForAllN {A} (P : N -> A -> Prop) (n : N) (s : Stream A) : Prop :=
  ForAllN_Cons {
    ForAllN_hd : P n (hd s);
    ForAllN_tl : ForAllN P (1 + n) (tl s);
  }.

Lemma ForAllN_const {A} (P : N -> A -> Prop) (a : A)
  : (forall n, P n a) ->
    (forall n, ForAllN P n (const a)).
Proof.
  intros H; cofix self; intros n.
  constructor; auto.
Qed.

Lemma _eval_correct (plates : list (list N))
    (nP : N) (qps ps : list N)
  : forall (i : N) (best : N) (acc : N) (qs : list N) (prev : Stream N),
    qps = qs ++ ps ->
    ForAllN (fun j => optimum plates (nP - j)) i prev ->
    (forall (xs : select_prefix qps) (xss : select_prefixes plates),
      value_prefix xs + value_prefixes xss <= best
      \/ exists ys : select_prefix ps,
         value_prefix xs = acc + value_prefix ys
         /\ size_prefix xs = N_of qs + size_prefix xs) ->
    optimum (qps :: plates) (nP - i) (_eval (nP - i) best acc ps prev).
Proof.
  induction ps; intros.
Admitted.

Lemma eval_correct (plates : list (list N))
    (nP : N) (i : N) (ps : list N) (prev : Stream N)
  : ForAllN (fun j => optimum plates (nP - j)) i prev ->
    optimum (ps :: plates) (nP - i) (eval (nP - i) ps prev).
Proof.
  intros Hprev.
  apply _eval_correct with (qps := ps) (qs := []);
    try auto.
  intros xs xss; right; exists xs.
  auto.
Qed.

Equations select_prefixes_nil_inv {A} (xs : select_prefixes [])
  : xs = @select_prefixes_nil A :=
  select_prefixes_nil_inv select_prefixes_nil := eq_refl.

Lemma optimum_nil nP
  : optimum [] nP 0.
Proof.
  split.
  - intros xss'. rewrite (select_prefixes_nil_inv xss'). reflexivity.
  - exists select_prefixes_nil.
    split; [ apply N.le_0_l | reflexivity ].
Qed.

Lemma _step_correct (plates : list (list N))
    (nP : N) (i : N) (ps : list N) (prev : Stream N)
  : ForAllN (fun j => optimum plates (nP - j)) i prev ->
    ForAllN (fun j => optimum (ps :: plates) (nP - j)) i (step (nP - i) ps prev).
Admitted.

Lemma step_correct (plates : list (list N))
    (nP : N) (ps : list N) (prev : Stream N)
  : ForAllN (fun j => optimum plates (nP - j)) 0 prev ->
    ForAllN (fun j => optimum (ps :: plates) (nP - j)) 0 (step nP ps prev).
Proof.
  replace (step nP) with (step (nP - 0)) by (f_equal; rewrite N.sub_0_r; reflexivity).
  apply _step_correct.
Qed.

Lemma best_values_correct (nP : N) (plates : list (list N))
  : ForAllN (fun j => optimum plates (nP - j)) 0%N (best_values nP plates).
Proof.
  induction plates; cbn.
  - apply ForAllN_const. intros; apply optimum_nil.
  - apply step_correct. auto.
Qed.

Lemma best_value_correct : CORRECT.
Proof.
  intros nP plates. destruct (best_values_correct nP plates).
  rewrite N.sub_0_r in *. assumption.
Qed.
