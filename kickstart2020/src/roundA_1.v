From Coq Require Import
  NArith
  String
  List.
From SimpleIO Require Import SimpleIO.
From Printf Require Import Printf.

From Lib Require Import common.

Import IO.Notations.
Import StringSyntax.
Import ListNotations.

Fixpoint _can_buy (a : N) (budget : N) (prices : list N) : N :=
  match prices with
  | [] => a
  | x :: xs =>
    if (x <=? budget)%N then
      _can_buy (1 + a) (budget - x) xs
    else
      a
  end.

Definition can_buy (budget : N) (prices : list N) : N :=
  _can_buy 0 budget (NSort.sort prices).

Definition main (_ : unit) : IO unit :=
  codejam_loop (fun i =>
    (* We can ignore nN *)
    nB <- scanf "%Nd %Nd" (fun _nN nB _ => Some nB) ;;
    nAs <- read_Ns ;;
    let y := can_buy nB nAs in
    print_endline (sprintf "Case #%Nd: %Nd" i y)).
