From Coq Require Import
  NArith
  List
  Permutation
  Sorting
  Lia.
From Coding Require Import roundA_1.

From Lib Require Import common.

Local Open Scope N_scope.

Fixpoint count (choice : list bool) : N :=
  match choice with
  | nil => 0
  | true :: choice => 1 + count choice
  | false :: choice => count choice
  end.

Fixpoint price_of_choice (choice : list bool) (prices : list N) : N :=
  match choice, prices with
  | true :: choice, p :: prices => p + price_of_choice choice prices
  | false :: choice, _ :: prices => price_of_choice choice prices
  | _, _ => 0
  end.

(* [can_buy] computes the maximum number of houses we can buy.
   We can describe the houses to buy with a vector of booleans [choice]:
   1. There must be a choice with the expected number of elements;
   2. Its total cost must be below the budget;
   3. And any other choice within the budget has fewer elements.
 *)

Definition CORRECT : Prop :=
  forall (budget : N) (prices : list N),
  exists (choice : list bool),
  length choice = length prices
  /\ count choice = can_buy budget prices       (* 1 *)
  /\ price_of_choice choice prices <= budget    (* 2 *)
  /\ forall (choice' : list bool),
    length choice' = length prices ->
    price_of_choice choice' prices <= budget ->
    count choice' <= count choice.              (* 3 *)

(**)

(* [can_buy] first sorts the list of prices.
   1. On a sorted list, the optimal list of choices is a prefix of it.
   2. A choice on a permutation of a list corresponds to a choice on the list itself,
      and we must transport the various properties through the permutation as
      well.
 *)

Lemma choice_permutation
  : forall prices prices',
    Permutation prices prices' ->
    forall choice,
    length choice = length prices ->
    exists choice',
    price_of_choice choice prices = price_of_choice choice' prices' /\
    Permutation choice choice'.
Proof.
  induction 1.
  - eexists; eauto.
  - intros [|c choice]; try discriminate.
    injection 1; intros.
    destruct (IHPermutation choice) as (choice' & Hc & HP); auto.
    exists (c :: choice'); cbn.
    rewrite Hc. auto.
  - intros [|c [|d choice]]; try discriminate.
    exists (d :: c :: choice). constructor.
    + cbn; destruct c, d; reflexivity + lia.
    + constructor.
  - intros choice Hc.
    destruct (IHPermutation1 choice Hc) as (choice' & Hprice' & HP').
    pose proof HP' as HQ. apply Permutation_length in HQ.
    apply Permutation_length in H.
    destruct (IHPermutation2 choice') as (choice'' & Hprice'' & HP'').
    { congruence. }
    exists choice''.
    constructor.
    + congruence.
    + econstructor; eauto.
Qed.

Lemma Proper_Sorted {A} (P Q : A -> A -> Prop)
    (HPQ : forall a b, P a b -> Q a b)
  : forall l,
    Sorted P l ->
    Sorted Q l.
Proof.
  induction 1 as [ | ? ? ? ? HR]; constructor; auto.
  induction HR; constructor; auto.
Qed.

Lemma sorted_sort
  : forall l, Sorted N.le (NSort.sort l).
Proof.
  intros.
  eapply Proper_Sorted;
    [| apply NSort.LocallySorted_sort].
  intros [] []; cbn; try discriminate.
  rewrite <- Pos.leb_compare.
  apply Pos.leb_le.
Qed.

Lemma _cheaper_prefix_choice choice
  : forall prices0 prices,
    length choice = length prices ->
    Sorted N.le (prices0 ++ prices) ->
    price_of_choice (N_repeat true (count choice)) (prices0 ++ prices) <= price_of_choice choice prices.
Proof.
  induction choice; cbn [count price_of_choice]; intros prices0 prices Hlen Hsorted; try reflexivity.
  destruct prices as [| p prices]; try discriminate.
  injection Hlen; intros Hlen1.
  destruct a.
  - rewrite N_repeat_add.
    destruct prices0 as [| q prices0]; cbn.
    + apply Sorted_inv in Hsorted. destruct Hsorted as [Hsorted HRel].
      specialize (IHchoice nil prices Hlen1 Hsorted). cbn in IHchoice.
      lia.
    + cbn in Hsorted.
      pose proof Hsorted as Hsorted'.
      apply Sorted_inv in Hsorted. destruct Hsorted as [Hsorted HRel].
      change (?a ++ ?b :: ?c) with  (a ++ (b :: nil) ++ c) in *.
      rewrite app_assoc in Hsorted.
      specialize (IHchoice (prices0 ++ p :: nil) prices Hlen1 Hsorted).
      rewrite <- app_assoc in IHchoice.
      apply N.add_le_mono; [ | assumption ].
      apply Sorted_extends in Hsorted'; [ | exact N.le_trans ].
      rewrite 2 Forall_app in Hsorted'.
      destruct Hsorted' as (_ & Hsorted' & _).
      apply Forall_inv in Hsorted'.
      assumption.
  - change (?a ++ ?b :: ?c) with  (a ++ (b :: nil) ++ c) in *.
    rewrite app_assoc in *.
    apply IHchoice; auto.
Qed.

Lemma cheaper_prefix_choice choice prices
  : length choice = length prices ->
    Sorted N.le prices ->
    price_of_choice (N_repeat true (count choice)) prices <= price_of_choice choice prices.
Proof.
  apply (_cheaper_prefix_choice choice nil).
Qed.

Fixpoint pad {A B} (a : A) (xs : list A) (ys : list B) : list A :=
  match xs, ys with
  | _, nil => nil
  | x :: xs, _ :: ys => x :: pad a xs ys
  | nil, _ :: ys => a :: pad a nil ys
  end.

Notation padrepeat n ys := (pad false (N_repeat true n) ys).

Lemma length_pad {A B} (a : A) (xs : list A) (ys : list B)
  : length (pad a xs ys) = length ys.
Proof.
  revert xs; induction ys; destruct xs; intros; cbn; f_equal; trivial.
Qed.

Lemma count_padrepeat {B} n (ys : list B)
  : n <= N_of ys ->
    count (padrepeat n ys) = n.
Proof.
  revert n; induction ys; [ | rewrite N_of_cons ]; intros.
  - apply N.le_0_r in H. subst; reflexivity.
  - destruct (N.zero_or_succ n) as [E0 | [m E1]]; subst n; cbn.
    + apply (IHys 0), N.le_0_l.
    + apply N.succ_le_mono in H.
      rewrite N_repeat_succ.
      cbn [count]. rewrite N.add_1_l.
      f_equal; eauto.
Qed.

Lemma price_of_choice_pad xs prices
  : price_of_choice (pad false xs prices) prices = price_of_choice xs prices.
Proof.
  revert xs; induction prices; destruct xs as [| []]; cbn; trivial.
  - apply (IHprices nil).
  - f_equal; trivial.
Qed.

Lemma _can_buy_cont prices
  : forall w budget,
    _can_buy w budget prices = w + _can_buy 0 budget prices.
Proof.
  induction prices; cbn [ _can_buy ]; intros.
  - rewrite N.add_0_r; reflexivity.
  - destruct (N.leb_spec0 a budget).
    + rewrite 2 (IHprices (1 + _)).
      lia.
    + rewrite N.add_0_r; reflexivity.
Qed.

Lemma __can_buy_max prices
  : forall n budget,
    price_of_choice (padrepeat n prices) prices <= budget ->
    count (padrepeat n prices) <= _can_buy 0 budget prices.
Proof.
  unfold can_buy.
  induction prices as [|p prices IHprices]; intros.
  - destruct N_repeat; cbn; reflexivity.
  - induction n using N.peano_ind.
    + cbn - [N.add] in *.
      change nil with (N_repeat true 0).
      rewrite count_padrepeat by apply N.le_0_l.
      apply N.le_0_l.
    + clear IHn. rewrite N_repeat_succ in *.
      cbn [ price_of_choice pad count _can_buy ] in *.
      destruct (N.leb_spec0 p budget).
      * cbn [pad count]. specialize (IHprices n (budget - p)).
        rewrite _can_buy_cont.
        lia.
      * exfalso; lia.
Qed.

Lemma _can_buy_max prices
  : forall n budget,
    price_of_choice (padrepeat n prices) prices <= budget ->
    count (padrepeat n prices) <= _can_buy 0 budget prices.
Proof.
  intros; rewrite <- (__can_buy_max _ n); auto. lia.
Qed.

Lemma _can_buy_length prices
  : forall w budget, _can_buy w budget prices <= w + N_of prices.
Proof.
  induction prices; intros; cbn [ _can_buy ].
  { rewrite N.add_0_r. reflexivity. }
  rewrite N_of_cons.
  destruct (N.leb_spec0 a budget).
  - rewrite <- N.add_1_l.
    specialize (IHprices (1 + w) (budget - a)).
    lia.
  - apply N.le_add_r.
Qed.

Lemma _can_buy_budget prices
  : forall budget, price_of_choice (padrepeat (_can_buy 0 budget prices) prices) prices <= budget.
Proof.
  induction prices; intros; cbn - [ N.add ].
  - apply N.le_0_l.
  - destruct (N.leb_spec0 a budget); cbn - [N.add].
    + rewrite _can_buy_cont. repeat rewrite N_repeat_add; cbn.
      specialize (IHprices (budget - a)).
      lia.
    + rewrite price_of_choice_pad. apply N.le_0_l.
Qed.

Lemma count_length xs
  : count xs <= N_of xs.
Proof.
  induction xs; cbn - [N.add N_of]; try reflexivity.
  rewrite N_of_cons, <- N.add_1_l.
  destruct a; lia.
Qed.

Lemma _can_buy_correct :
  forall (budget : N) (prices : list N),
  Sorted N.le prices ->
  exists (choice : list bool),
  length choice = length prices
  /\ count choice = _can_buy 0 budget prices
  /\ price_of_choice choice prices <= budget
  /\ forall (choice' : list bool),
    length choice' = length prices ->
    price_of_choice choice' prices <= budget ->
    count choice' <= count choice.
Proof.
  intros budget prices Hsorted.
  exists (padrepeat (_can_buy 0 budget prices) prices).
  split; [| split; [|split]].
  - apply length_pad.
  - apply count_padrepeat.
    apply _can_buy_length.
  - apply _can_buy_budget.
  - intros choice' Hlen Hprice.
    rewrite count_padrepeat by (apply _can_buy_length).
    rewrite <- (count_padrepeat _ prices).
    2: rewrite count_length; apply N.eq_le_incl, N_eq_length; trivial.
    apply _can_buy_max.
    trivial.
    etransitivity.
    2: apply Hprice.
    + rewrite price_of_choice_pad.
      apply cheaper_prefix_choice; trivial.
Qed.

Lemma Permutation_count xs ys
  : Permutation xs ys -> count xs = count ys.
Proof.
  induction 1; cbn - [N.add]; trivial.
  - rewrite IHPermutation; reflexivity.
  - destruct x, y; reflexivity.
  - congruence.
Qed.

Ltac choice_permutation choice0 choice1 :=
  let CP := fresh "CP" in
  let Hprice := fresh "Hprice" choice1 in
  let Hperm := fresh "Hperm" choice1 in
  pose proof choice_permutation as CP;
  specialize CP with (choice := choice0);
  edestruct CP as (choice1 & Hprice & Hperm);
    clear CP;
    [ | eassumption | ];
    [  match goal with
       | [ |- Permutation (NSort.sort _) _ ] => symmetry
       | [ |- ?x ] => idtac
       end; solve [ apply NSort.Permuted_sort ]
    | ];
    assert (count choice0 = count choice1) by (apply Permutation_count; assumption);
    assert (length choice0 = length choice1) by (apply Permutation_length; assumption).

Theorem can_buy_correct : CORRECT.
Proof.
  red. intros budget prices.
  assert (Hlenp : length prices = length (NSort.sort prices)).
  { apply Permutation_length, NSort.Permuted_sort. }
  destruct (_can_buy_correct budget (NSort.sort prices)) as (choice0 & Hlen0 & Hcount0 & Hprice0 & Hmin0).
  { apply sorted_sort. }
  choice_permutation choice0 choice.
  exists choice.
  unfold can_buy.
  split; [| split; [| split ]]; try congruence.
  intros choice' ? ?.
  choice_permutation choice' choice0'.
  specialize (Hmin0 choice0').
  apply (fun H I K => K (Hmin0 H I)); congruence.
Qed.
