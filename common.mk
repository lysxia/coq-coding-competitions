.PRECIOUS: build/%.ml src/%.vo build/%.native
.PHONY: %.run lib

COQLIBDIR := ../lib
COQLIBPREFIX := Lib
COQPREFIX := Coding

COQLIB := common.v
COQLIBVO := $(patsubst %.v, $(COQLIBDIR)/%.vo, $(COQLIB))

%.run: build/%.native
	$< < test/$*.input | tee test/$*.output
	if [ -e test/$*.ref ] ; then diff test/$*.ref test/$*.output ; fi

build:
	mkdir -p build/

fifo1 fifo2:
	mkfifo fifo1 fifo2

build/%.ml: build src/%.vo src/%_extract.v
	cd build/ && coqc -Q ../$(COQLIBDIR) $(COQLIBPREFIX) -Q ../src $(COQPREFIX) ../src/$*_extract.v
	rm build/$*.mli

build/%_test.native: build build/%.native
	cd build/ && coqc -Q ../$(COQLIBDIR) $(COQLIBPREFIX) -Q ../src $(COQPREFIX) ../src/$*_test.v
	cd build/ && ocamlbuild $*_test.native

src/%.vo: $(COQLIBDIR)/common.vo src/%.v src/%_proof.v
	coqc -Q $(COQLIBDIR) $(COQLIBPREFIX) -Q src/ $(COQPREFIX) src/$*.v
	coqc -Q $(COQLIBDIR) $(COQLIBPREFIX) -Q src/ $(COQPREFIX) src/$*_proof.v

build/%.native: build/%.ml
	cd build/ && ocamlbuild $*.native

lib: clean
	$(MAKE) -C ../lib

$(COQLIBDIR)/common.vo:
	$(MAKE) lib

clean:
	rm -rf build/*
	rm -f fifo1 fifo2 src/*.{vo,vok,vos,glob} src/.*.aux .nia.cache .lia.cache

cleanall: clean
	$(MAKE) -C ../lib cleanall
	rm -rf build/
