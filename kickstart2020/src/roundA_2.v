From Coq Require Import
  NArith
  List
  Streams.
From SimpleIO Require Import SimpleIO.
From Printf Require Import Printf.

From Lib Require Import common.

Import ListNotations.
Import IO.Notations.

Local Open Scope N_scope.

Declare Scope stream_scope.
Local Open Scope stream_scope.
Infix "::" := Cons : stream_scope.

(* We use a stream because it gets extracted to a lazy stream *)

Fixpoint _eval (nP : N) (best : N) (acc : N) (ps : list N) (prev : Stream N) : N :=
  let best := N.max best (acc + hd prev) in
  if N.eqb nP 0 then
    best
  else
    match ps with
    | [] => best
    | (p :: ps)%list => _eval (nP - 1) best (p + acc) ps (tl prev)
    end.

Definition eval (nP : N) (ps : list N) (prev : Stream N) : N :=
  _eval nP 0 0 ps prev.

Example eval_example_1
  : eval 3 [2;1;1] (3 :: 2 :: 1 :: const 0) = 4.
Proof. reflexivity. Qed.

Example eval_example_2
  : eval 3 [1;1;1] (4 :: 2 :: 1 :: const 0) = 4.
Proof. reflexivity. Qed.

Example eval_example_3
  : eval 3 [1;2;1] (3 :: 2 :: 1 :: const 0) = 4.
Proof. reflexivity. Qed.

CoFixpoint step (nP : N) (ps : list N) (prev : Stream N) : Stream N :=
  if N.eqb nP 0 then
    const 0
  else
    eval nP ps prev :: step (nP - 1) ps (tl prev).

Fixpoint take {A} (n : nat) (xs : Stream A) : list A :=
  match n with
  | O => []
  | S n => hd xs :: take n (tl xs)
  end.

Example step_example_1
  : take 5 (step 5 [10; 10; 100; 30] (const 0)) = [150; 150; 120; 20; 10].
Proof. reflexivity. Qed.

Example step_example_2
  : take 5 (step 5 [80; 50; 10; 30] (150 :: 150 :: 120 :: 20 :: 10 :: const 0))
  = [250; 200; 140; 130; 80].
Proof. reflexivity. Qed.

Fixpoint best_values (nP : N) (plates : list (list N)) : Stream N :=
  match plates with
  | [] => const 0
  | ps :: plates =>
    step nP ps (best_values nP plates)
  end%list.

Definition best_value (nP : N) (plates : list (list N)) : N :=
  Streams.hd (best_values nP plates).

Definition main (_ : unit) : IO unit :=
  codejam_loop (fun i =>
    nn <- scanf "%Nd %Nd %Nd" (fun nN _nK nP _ => Some (nN, nP)) ;;
    let '(nN, nP) := nn in
    plates <- N.iter nN (fun go rs =>
      r <- read_Ns ;;
      go (r :: rs)%list) (fun rs => IO.ret rs) [] ;;
    let y := best_value nP plates in
    print_endline (sprintf "Case #%Nd: %Nd" i y)).
