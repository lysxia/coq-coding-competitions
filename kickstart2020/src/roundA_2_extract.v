From SimpleIO Require Import SimpleIO.
From Coding Require Import roundA_2.

Definition run_main : io_unit := IO.unsafe_run (main tt).
Extraction "roundA_2.ml" run_main.
