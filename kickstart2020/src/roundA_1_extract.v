From SimpleIO Require Import SimpleIO.
From Coding Require Import roundA_1.

Definition run_main : io_unit := IO.unsafe_run (main tt).
Extraction "roundA_1.ml" run_main.
