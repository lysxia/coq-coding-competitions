From Coq Require Import
  ExtrOcamlIntConv
  Arith
  NArith
  Ascii
  String
  List
  Orders
  OrdersEx
  Sorting.
From SimpleIO Require Import SimpleIO.
From Printf Require Import Printf Scanf.

Import ListNotations.

(** * Core assumptions *)

(** ** Characters *)

Parameter char_eqb_eq : forall c1 c2, char_eqb c1 c2 = true <-> c1 = c2.

Parameter char_as_nat : char -> nat -> Prop.

Parameter char_as_nat_0 : char_as_nat "0"%char 0.
Parameter char_as_nat_2 : char_as_nat "2"%char 2.
Parameter char_as_nat_4 : char_as_nat "4"%char 4.

Parameter char_as_nat_injective : forall c n1 n2,
  char_as_nat c n1 ->
  char_as_nat c n2 ->
  n1 = n2.

Parameter ascii_char_injective : forall a1 a2 : ascii,
  (a1 : char) = (a2 : char) -> a1 = a2.

(** * Useful functions *)

Local Open Scope N_scope.

(** ** Small stuff *)

(** Convert a [bool] to an [N] *)
Definition N_bit (b : bool) : N :=
  if b then 1 else 0.

(** ** Option *)

Definition option_map {A B} (f : A -> B) (ox : option A) : option B :=
  match ox with
  | Some x => Some (f x)
  | None => None
  end.

Definition option_bind {A B} (ox : option A) (k : A -> option B) : option B :=
  match ox with
  | Some x => k x
  | None => None
  end.

(** ** IO *)

Import IO.Notations.

Definition unwrap {A} (o : option A) : IO A :=
  match o with
  | Some x => IO.ret x
  | None => exit (int_of_n 1%N)
  end.

(** Read an integer [T] and run the [body] that many times,
    incrementing a counter for each iteration, starting from 1. *)
Definition codejam_loop (x : N -> IO unit) : IO unit :=
  n <- IO.map n_of_int read_int ;;
  N.iter n (fun go i => x i ;; go (i + 1)%N) (fun _ => IO.ret tt) 1.

(** Read one [N] on one line. Must not exceed the size of one OCaml [int]. *)
Definition read_N : IO N :=
  IO.map n_of_int read_int.

(* Extraction trick to monomorphize things. *)
Definition as_type_of {A} (_ b : A) : A := b.
Extract Inlined Constant as_type_of => "(fun (_ : 'a) (y : 'a) -> y)".

(** Read a list of [N] on one line separated by spaces. *)
Definition read_Ns' (_ : unit) : IO (list N) :=
  s <- read_line' ;;
  IO.fix_io (fun parse '(acc, s) =>
    match sscanf "%Nd " (fun x s => Some (x, s)) s with
    | Some (x, s) => parse (x :: acc, s)
    | None => IO.ret (as_type_of [0%N] (List.rev' acc))
    end) ([], s).

(** Read a list of [N] on one line separated by spaces,
    failing loudly if the line doesn't look right. *)
Definition read_Ns : IO (list N) :=
  xs <- read_Ns' tt ;;
  match as_type_of [0%N] xs with
  | [] => exit (int_of_n 1)  (* Something is probably wrong *)
  | _ :: _ => IO.ret xs
  end.

(** Read ONE LINE according to a format string. *)
Definition scanf {R : Type} (fmt : Printf.Format.Format.t)
  : Format.Format.holes (string -> option R) fmt -> IO R :=
  fun k =>
    s <- read_line' ;;
    match Scanf.sscanf fmt k s with
    | None => exit (int_of_n 1)  (* Failed parsing *)
    | Some r => IO.ret r
    end.

(** ** Iteration with [N] *)

Definition N_repeat {A : Type} (a : A) (n : N) : list A :=
  N.iter n (fun xs => a :: xs) [].

Definition N_repeatf {A} (f : A -> A) (a : A) (n : N) : list A :=
  N.iter n (fun go a => a :: go (f a)) (fun _ => []) a.

(** Length as an [N] *)
Definition N_of {A} (xs : list A) : N :=
  List.fold_left (fun i _ => N.succ i) xs 0%N.

(** ** List combinators *)

Fixpoint list_eqb {A B} (eqb_ : A -> B -> bool) (xs : list A) (ys : list B) : bool :=
  match xs, ys with
  | [], [] => true
  | x :: xs, y :: ys => eqb_ x y && list_eqb eqb_ xs ys
  | _ :: _, [] | [], _ :: _ => false
  end.

Fixpoint zip_with {A B C : Type} (f : A -> B -> C) (xs : list A) (ys : list B) : list C :=
  match xs, ys with
  | [], _ | _, [] => []
  | x :: xs, y :: ys => f x y :: zip_with f xs ys
  end.

Fixpoint transpose {A : Type} (xss : list (list A)) : list (list A) :=
  match xss with
  | [] => []
  | [xs] => map (fun x => [x]) xs
  | xs :: xss => zip_with cons xs (transpose xss)
  end.

Fixpoint tails {A} (xs : list A) : list (list A) :=
  xs ::
    match xs with
    | [] => []
    | _ :: xs => tails xs
    end.

Definition tails' {A} (xs : list A) : list (list A) :=
  match xs with
  | [] => []
  | _ :: xs => tails xs
  end.

(** Number of [true] bits in a list. *)
Definition B_of (xs : list bool) : N :=
  List.fold_left (fun i (b : bool) => if b then N.succ i else i) xs 0%N.

(** * Sorting *)

Module NOTF := OT_to_Full N_as_OT.
Module NTTLB := OTF_to_TTLB NOTF.
Module NSort := Sort NTTLB.

(** * General facts *)

Lemma char_neqb_neq c1 c2
  : char_eqb c1 c2 = false <-> c1 <> c2.
Proof.
  split; intros H.
  - intros I. apply char_eqb_eq in I. rewrite I in H. discriminate.
  - apply Bool.not_true_iff_false.
    intros I. apply char_eqb_eq in I. contradiction.
Qed.

Lemma ascii_char_neq : forall a1 a2 : ascii,
  a1 <> a2 -> (a1 : char) <> (a2 : char).
Proof.
  intros a1 a2 H I; apply ascii_char_injective in I; contradiction.
Qed.

Lemma N_iter_succ_r {A} (f : A -> A) (a : A) (n : N)
  : N.iter (N.succ n) f a = N.iter n f (f a).
Proof.
  rewrite 2 N2Nat.inj_iter, N2Nat.inj_succ.
  unfold Nat.iter.
  rewrite nat_rect_succ_r.
  reflexivity.
Qed.

Lemma N_iter_succ_l {A} (f : A -> A) (a : A) (n : N)
  : N.iter (N.succ n) f a = f (N.iter n f a).
Proof.
  rewrite 2 N2Nat.inj_iter, N2Nat.inj_succ.
  unfold Nat.iter.
  apply (nat_rect_plus 1).
Qed.

Lemma N_of_cons {A} (x : A) (xs : list A)
  : N_of (x :: xs) = N.succ (N_of xs).
Proof.
  unfold N_of. cbn.
  enough (H : forall i,
    fold_left (fun i _ => N.succ i) xs (N.succ i) =
    N.succ (fold_left (fun i _ => N.succ i) xs i)).
  { apply (H 0%N). }
  induction xs; intros; cbn.
  - reflexivity.
  - rewrite IHxs. reflexivity.
Qed.

Lemma Forall_inv_cons {A} (P : A -> Prop) (x : A) (xs : list A)
  : Forall P (x :: xs) -> P x /\ Forall P xs.
Proof.
  split; [ eapply Forall_inv; eauto | eauto using Forall_inv_tail ].
Qed.

Lemma Forall_app {A} (P : A -> Prop) (xs ys : list A)
  : Forall P (xs ++ ys) <-> Forall P xs /\ Forall P ys.
Proof.
Admitted.

Lemma Forall_rev {A} (P : A -> Prop) (xs : list A)
  : Forall P xs -> Forall P (rev xs).
Proof.
  intros H. apply Forall_forall. rewrite Forall_forall in H.
  intros x I. apply in_rev in I; auto.
Qed.

Lemma repeatf_succ {A} (f : A -> A) (a : A) (n : N)
  : N_repeatf f a (N.succ n) = a :: N_repeatf f (f a) n.
Proof.
  unfold N_repeatf; rewrite N_iter_succ_l; reflexivity.
Qed.

Lemma zip_with_nil_r {A B C} (f : A -> B -> C) (xs : list A)
  : zip_with f xs [] = [].
Proof.
  destruct xs; reflexivity.
Qed.

Lemma N_of_repeat {A} (a : A) (n : N) : N_of (N_repeat a n) = n.
Proof.
  unfold N_repeat.
  induction n using N.peano_ind; [ reflexivity |].
  rewrite N_iter_succ_l, N_of_cons, IHn. reflexivity.
Qed.

Lemma N_of_repeatf {A} (n : N)
  : forall (f : A -> A) (a : A),
      N_of (N_repeatf f a n) = n.
Proof.
  unfold N_repeatf.
  induction n using N.peano_ind; [ reflexivity |]; intros.
  rewrite N_iter_succ_l, N_of_cons, IHn. reflexivity.
Qed.

Lemma N_of_nat_length {A} (xs : list A) : N_of xs = N.of_nat (List.length xs).
Proof.
  induction xs; [ reflexivity | rewrite N_of_cons ].
  cbn [ List.length ]. rewrite Nat2N.inj_succ, IHxs.
  reflexivity.
Qed.

Lemma N_to_nat_length {A} (xs : list A) : List.length xs = N.to_nat (N_of xs).
Proof.
  rewrite N_of_nat_length, Nat2N.id. reflexivity.
Qed.

Lemma N_eq_length {A B} (xs : list A) (ys : list B)
  : N_of xs = N_of ys <->
    List.length xs = List.length ys.
Proof.
  split.
  - rewrite 2 N_to_nat_length; intros []; auto.
  - rewrite 2 N_of_nat_length; intros []; auto.
Qed.

Lemma transpose_singleton {A} (x : A) (xs : list A)
  : transpose ([x] :: map (fun x => [x]) xs) = [x :: xs].
Proof.
  revert x; induction xs; intros x.
  - reflexivity.
  - cbn [ map transpose ] in *. rewrite IHxs. reflexivity.
Qed.

Inductive zip_with_case {A B C} (f : A -> B -> C)
  : list A -> list B -> list C -> Prop :=
| zip_with_nil : zip_with_case f [] [] []
| zip_with_cons x xs y ys
  : N_of xs = N_of ys ->
    zip_with_case f xs ys (zip_with f xs ys) ->
    zip_with_case f (x :: xs) (y :: ys) (f x y :: zip_with f xs ys)
.

Lemma zip_with_split {A B C} (f : A -> B -> C) (xs : list A) (ys : list B)
  : N_of xs = N_of ys ->
    zip_with_case f xs ys (zip_with f xs ys).
Proof.
  revert ys; induction xs; intros ys H; apply N_eq_length in H;
    destruct ys; try discriminate.
  - constructor.
  - apply Nat.succ_inj, N_eq_length in H; constructor; auto.
Qed.

Lemma transpose_zip_cons {A} (n : N) (x : list A) xs
  : N_of x = N.succ n ->
    N_of x = N_of xs ->
    transpose (zip_with cons x xs) = x :: transpose xs.
Proof.
  revert n xs. induction x as [ | i is ]; intros n xs Hx Hxs.
  { apply N.neq_0_succ in Hx; contradiction. }
  apply N_eq_length in Hxs. destruct xs as [ | y ys ] ; try discriminate.
  cbn [ zip_with ].
  rewrite N_of_cons in Hx. apply N.succ_inj in Hx.
  apply Nat.succ_inj, N_eq_length in Hxs.
  specialize (IHis (N.pred n) ys).
  destruct (zip_with_split cons is ys Hxs).
  - reflexivity.
  - repeat match goal with
           | [ H : _ |- _ ] => rewrite !N_of_cons in H
           end.
    rewrite <- Hx, N.pred_succ in IHis.
    cbn [ transpose ] in *.
    rewrite IHis; auto.
Qed.

Lemma N_of_map {A B} (f : A -> B) (xs : list A)
  : N_of (map f xs) = N_of xs.
Proof.
  induction xs; [ reflexivity | cbn [ map ]; rewrite 2 N_of_cons, IHxs; reflexivity ].
Qed.

Lemma N_of_zip_with {A B C} (f : A -> B -> C) (xs : list A) (ys : list B)
  : N_of xs = N_of ys -> N_of (zip_with f xs ys) = N_of xs.
Proof.
  intros H; apply N_eq_length in H; revert ys H.
  induction xs; destruct ys; try discriminate + reflexivity.
  cbn [ List.length zip_with ]; intros H; apply Nat.succ_inj in H.
  rewrite N_of_cons, IHxs, N_of_cons; auto.
Qed.

Lemma N_of_transpose {A} (n : N) (x : list A) (xs : list (list A))
  : Forall (fun r => N_of r = n) (x :: xs) ->
    N_of (transpose (x :: xs)) = n.
Proof.
  revert x; induction xs as [ | y ys IHys ]; intros x H.
  - unfold transpose; rewrite N_of_map. apply Forall_inv in H. assumption.
  - cbn [ transpose ] in *.
    apply Forall_inv_cons in H; destruct H as [ H0 H ].
    apply Forall_inv_cons in H; destruct H as [ H1 H ].
    rewrite N_of_zip_with; auto.
    destruct ys.
    + rewrite N_of_map; subst; auto.
    + rewrite IHys; auto.
Qed.

Lemma positive_length {A} (n : N) (xs : list A)
  : N_of xs = N.succ n -> exists x xs', xs = x :: xs'.
Proof.
  destruct xs; intros H.
  - apply N.neq_0_succ in H; contradiction.
  - eauto.
Qed.

Lemma Forall_repeatf {A} (P : A -> Prop) (f : A -> A)
  : (forall a, P a -> P (f a)) ->
    forall n a, P a -> Forall P (N_repeatf f a n).
Proof.
  intros H.
  induction n using N.peano_ind; intros; [ constructor | ].
  rewrite repeatf_succ. constructor; auto.
Qed.

Lemma unfold_tails {A} (xs : list A) : tails xs = xs :: tails' xs.
Proof. destruct xs; reflexivity. Qed.

Lemma list_eqb_spec {A} (eqb_ : A -> A -> bool)
    (eqb_spec_ : forall a b, BoolSpec (a = b) (a <> b) (eqb_ a b))
  : forall xs ys, BoolSpec (xs = ys) (xs <> ys) (list_eqb eqb_ xs ys).
Proof.
  induction xs as [ | x xs IHxs ]; destruct ys as [ | y ys ]; cbn;
    try (constructor; reflexivity + discriminate).
  destruct (eqb_spec_ x y) as [ [] | ]; [ destruct (IHxs ys) as [ [] | ] | ]; constructor;
    reflexivity + (injection; contradiction).
Qed.

Lemma bool_list_eqb_spec xs ys
  : BoolSpec (xs = ys) (xs <> ys) (list_eqb Bool.eqb xs ys).
Proof.
  apply list_eqb_spec.
  intros [] []; constructor; reflexivity + discriminate.
Qed.

Lemma Forall_tails_repeatf {A} (Q : A -> Prop) (P : list A -> Prop) (f : A -> A)
  : (forall a, Q a -> Q (f a)) ->
    (forall n a, Q a -> P (N_repeatf f a n)) ->
    forall n a, Q a -> Forall P (tails (N_repeatf f a n)).
Proof.
  intros G H n. induction n using N.peano_ind; intros a; cbn.
  - constructor; [ apply (H 0 a); auto | constructor ].
  - rewrite unfold_tails. unfold N_repeatf at 2. rewrite N_iter_succ_l.
    constructor; [ apply H | apply IHn ]; auto.
Qed.

Lemma N_iter_comm {A B C} (n : N) (a : A) (b : B) (g : A -> B -> C)
    (fa : A -> A) (fb : B -> B) (fc : C -> C)
  : (forall x y, fc (g x y) = g (fa x) (fb y)) ->
    N.iter n fc (g a b) = g (N.iter n fa a) (N.iter n fb b).
Proof.
  intros H. revert a b. induction n using N.peano_ind; [ reflexivity |].
  intros; rewrite 3 N_iter_succ_r, H, IHn; reflexivity.
Qed.

Lemma N_double_add2 (n : N) : N.double n = n + n.
Proof.
  rewrite N.double_spec.
  change 2 with (1 + 1).
  rewrite N.mul_add_distr_r, N.mul_1_l.
  reflexivity.
Qed.

Lemma N_double_succ (n : N) : N.double (N.succ n) = N.succ (N.succ (N.double n)).
Proof.
  rewrite !N_double_add2.
  rewrite <- !N.add_1_l.
  rewrite <- N.add_assoc, (N.add_assoc n 1), (N.add_comm n 1), <- N.add_assoc.
  reflexivity.
Qed.

Lemma N_iter_add {A} (n m : N) (f : A -> A) (a : A)
  : N.iter (n + m) f a = N.iter n f (N.iter m f a).
Proof.
  induction n using N.peano_ind; intros; [ reflexivity |].
  rewrite N.add_succ_l, 2 N_iter_succ_l. f_equal; assumption.
Qed.

Lemma N_iter_double {A} (n : N) (f : A -> A) (a : A)
  : N.iter (N.double n) f a = N.iter n f (N.iter n f a).
Proof.
  rewrite N_double_add2, N_iter_add; reflexivity.
Qed.

Lemma N_repeat_succ {A} (a : A) (n : N)
  : N_repeat a (N.succ n) = a :: N_repeat a n.
Proof.
  apply N_iter_succ_l.
Qed.

Lemma N_repeat_add {A} (a : A) (n m : N)
  : N_repeat a (n + m) = N_repeat a n ++ N_repeat a m.
Proof.
  induction n using N.peano_ind.
  - reflexivity.
  - rewrite N.add_succ_l, 2 N_repeat_succ, IHn.
    reflexivity.
Qed.

Lemma transpose_transpose {A} (n : N) (m : list (list A))
  : Forall (fun r => N_of r = N.succ n) m ->
    transpose (transpose m) = m.
Proof.
  induction 1.
  - reflexivity.
  - cbn. destruct l.
    + destruct (positive_length _ _ H) as (? & ? & ?). subst; eapply transpose_singleton; eassumption.
    + erewrite transpose_zip_cons; eauto.
      { rewrite IHForall. reflexivity. }
      { erewrite N_of_transpose; eauto. }
Qed.

Lemma B_of_cons (x : bool) (xs : list bool)
  : B_of (x :: xs) = N_bit x + B_of xs.
Proof.
  cbn.
  enough (H : forall a,
    fold_left (fun i (b : bool) => if b then N.succ i else i) xs
      a = a + B_of xs).
  { apply H. }
  induction xs; intros; cbn.
  - rewrite N.add_0_r. reflexivity.
  - rewrite 2 IHxs. destruct a.
    + rewrite <- N.add_1_r, N.add_assoc. reflexivity.
    + reflexivity.
Qed.
